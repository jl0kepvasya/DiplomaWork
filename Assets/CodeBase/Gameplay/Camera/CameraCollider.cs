using UnityEngine;
using Zenject;

//Приближение камеры при столкновении с объектами и следование камеры за игроком
public class CameraCollider : MonoBehaviour
{
    #region Variables
    [Header("Check colliders")]
    [SerializeField] private float _checkCollidersRadius;
    [SerializeField] private LayerMask _checkCollidersMasks;

    [Header("Offset approach")] 
    [SerializeField] private float _offset;
    [SerializeField] private float _minOffset;

    [Header("Follow object")] 
    [SerializeField] private float _camFollowSpeed;
    [Inject] private Transform _followObj;
    private Vector3 _camFollowVelocity;

    private Camera _cam;
    private RaycastHit _hit;
    private Vector3 _defaultPosition;
    private Vector3 _vectorPosition;
    #endregion
    
    private void Awake()
    {
        _cam = Camera.main;
        _defaultPosition = _cam.transform.localPosition;
        _vectorPosition = new Vector3(_defaultPosition.x, _defaultPosition.y, _defaultPosition.z);
        _camFollowVelocity = Vector3.zero;
    }

    private void Update()
    {
        FollowTarget();
    }

    private void FixedUpdate()
    {
        CameraApproach();
    }

    private void FollowTarget()
    {
        Vector3 targetPosition = Vector3.SmoothDamp(
            transform.position, _followObj.position, ref _camFollowVelocity, _camFollowSpeed);
        
        transform.position = targetPosition;
    }

    private void CameraApproach()
    {
        float targetPosition = _defaultPosition.z;
        Vector3 direction = _cam.transform.position - transform.position;
        direction.Normalize();

        if (isCollide(targetPosition, direction))
        {
            float distance = Vector3.Distance(transform.position, _hit.point);
            targetPosition -= distance - _offset;
        }
        if (Mathf.Abs(targetPosition) < _minOffset)
            targetPosition -= _minOffset;
            
        
        _vectorPosition.z = Mathf.Lerp(_cam.transform.localPosition.z, targetPosition, 0.2f);
        _cam.transform.localPosition = _vectorPosition;
    }

    private bool isCollide(float targetPosition, Vector3 direction)
    {
        return Physics.SphereCast(transform.position, _checkCollidersRadius, direction, out _hit,
            Mathf.Abs(targetPosition), _checkCollidersMasks);
    }
}