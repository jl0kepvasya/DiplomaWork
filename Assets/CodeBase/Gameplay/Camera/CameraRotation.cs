using UnityEngine;
using Zenject;

//Поворот камеры вслед за движением мышки
public class CameraRotation : MonoBehaviour
{
    [Inject] private InputSystem _input;
    
    [SerializeField] private float _clampMax, _clampMin;
    [SerializeField] private CameraRotationConfig _sensivity;
    
    private float _horizontalAngle;
    private float _verticalAngle;

    private void Update()
    {
        CameraRotate();
    }

    private void CameraRotate()
    {
        Vector2 mouseDirection = _input.GetVector2Variable("MousePos");
        
        _horizontalAngle -= -mouseDirection.x * _sensivity.X;
        _verticalAngle -= mouseDirection.y * _sensivity.Y;
        
        Vector3 rotation = Vector3.zero;
        rotation.y = _horizontalAngle;
        rotation.x = Mathf.Clamp(_verticalAngle, _clampMin, _clampMax);
        Quaternion targetRotation = Quaternion.Euler(rotation);
        transform.localRotation = targetRotation;
    }
}