using UnityEngine;

[CreateAssetMenu(menuName = "Player/Camera Sensivity")]
public class CameraRotationConfig : ScriptableObject
{
    [field: SerializeField] public float X;
    [field: SerializeField] public float Y;
}