using UnityEngine;

public class AttackSO : ScriptableObject
{
    [field: SerializeField] public AnimatorOverrideController AnimatorOV;
    public float currentWeaponDamage;
}