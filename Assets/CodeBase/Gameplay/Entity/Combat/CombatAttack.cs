using System.Collections.Generic;
using UnityEngine;

public abstract class CombatAttack : MonoBehaviour
{
    public List<AttackSO> Combo;
    public int ComboCounter;
    public float LastComboEnd, LastAttackTime;

    public Animator Anim;

    private void Update()
    {
        ExitAttack();
    }

    public void Attack()
    {
        if (Time.deltaTime - LastComboEnd > .5f && ComboCounter < Combo.Count)
        {
            CancelInvoke("EndCombo");

            if (Time.deltaTime - LastAttackTime >= .2f)
            {
                Anim.runtimeAnimatorController = Combo[ComboCounter].AnimatorOV;
                Anim.Play("Attack", 0, 0);

                if (ComboCounter > Combo.Count)
                    ComboCounter = 0;
            }
        }
    }

    public void ExitAttack()
    {
        if (Anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= .9f &&
            Anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            Invoke("EndCombo", 1);
        }
    }
    
    public void EndCombo()
    {
        ComboCounter = 0;
        LastComboEnd = Time.deltaTime;
    }
}