using System;

public abstract class CombatBlock
{
    public float BlockPercentage;
    public Action<float> DamageBlocked;
    
    public void Block(ref float damage)
    {
        damage -= damage * BlockPercentage / 100;
        DamageBlocked?.Invoke(damage);
    }
}