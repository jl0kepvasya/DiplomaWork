using UnityEngine;

//Конфиг для существ со взаимодействием
[CreateAssetMenu(menuName = "Entity/InteractSO")]
public class EntityInteractConfig : ScriptableObject
{
    [Header("Interact variables")]
    [SerializeField] public float InteractRadius;
    [SerializeField] public LayerMask InteractablesLayer;
}