using System;
using UnityEngine;

//Конфиг для существ, которые двигаются
[CreateAssetMenu(menuName = "Entity/MovementSO")]
public class EntityMovementConfig : ScriptableObject
{
    [Header("Movement variables")]
    [field: SerializeField] public float MoveSpeed;
    [field: SerializeField] public float RunSpeed;
    [field: SerializeField] public float RotationSpeed;
    [field: SerializeField] public float Gravity;
    [field: SerializeField] public float JumpForce;
    [field: NonSerialized] public float VerticalVelocity;
    
    [Header("Ground check")]
    [SerializeField] public float GroundCheckRadius;
    [SerializeField] public LayerMask GroundLayers;
}