using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Barrel : Interactable
{
    [Header("Health")] 
    [SerializeField] private float _startHealth;
    [SerializeField] private HealthView _healthView;
    
    private Health _health;
    
    [Header("Inventory")]
    [SerializeField] private string _ownerId;
    [SerializeField] private ScreenView _screenView;
    
    private InventorySlots _inventorySlots;
    private InventoriesService _service;
    private ScreenController _screenController;

    [Inject] private Dictionary<string, ItemSO> _items;
    
    private void Awake()
    {
        //Health
        HealthData healthData = new HealthData()
        {
            StartHealthPoints = _startHealth,
            CurrentHealthPoints = _startHealth
        };
        
        _health = new Health(healthData);

        HealthController healthController = new HealthController(_health, _healthView);
        
        //Inventory
        _inventorySlots = new InventorySlots();
        _service = new InventoriesService();
        
        var invData = _inventorySlots.CreateInventory(_ownerId, new Vector2Int(2,2));
        _service.RegisterInventory(invData, _items);

        _screenController = new ScreenController(_screenView, _service);
    }

    protected override void Interact()
    {
        _screenController.OpenInventory(_ownerId);
    }
}