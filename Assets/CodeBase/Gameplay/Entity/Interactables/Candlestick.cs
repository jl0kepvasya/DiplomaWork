using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//Одна из возможных реализаций предметов взаимодействия
public class Candlestick : Interactable
{
    private List<Light> _lights;

    private void Awake() =>
        _lights = GetComponentsInChildren<Light>().ToList();

    protected override void Interact()
    {
        foreach (var light in _lights)
            light.gameObject.SetActive(!light.gameObject.activeInHierarchy);
    }
}