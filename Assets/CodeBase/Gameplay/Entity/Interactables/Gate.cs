using UnityEngine;

public class Gate : Interactable
{
    [SerializeField] private float maxGate;
    
    protected override void Interact()
    {
        Vector3 objPosition = transform.position;
        transform.position = new Vector3(objPosition.x, 
            maxGate,
            objPosition.z);
    }
}