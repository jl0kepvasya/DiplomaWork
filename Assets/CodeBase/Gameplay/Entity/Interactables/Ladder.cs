using UnityEngine;
using UnityEngine.SceneManagement;

public class Ladder : Interactable
{
    [SerializeField] private int _scene;
    
    protected override void Interact() =>
        SceneManager.LoadScene(_scene);
}