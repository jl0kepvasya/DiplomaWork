//Интерфейс для движения игрока

public interface IControllable
{
    public void Move();
    public InputPerform Jump();
}