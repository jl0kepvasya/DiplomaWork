using UnityEngine;
using Zenject;

//Движение игрока
[RequireComponent(typeof(Rigidbody))]
public class Movement : MonoBehaviour, IControllable
{
    #region Variables
    [Header("Config")]
    [SerializeField] private EntityMovementConfig _data;
    [SerializeField] private GameObject _groundCheckPivot;
    
    [Inject] private InputSystem _input;

    private Rigidbody _rigidbody;
    private Transform _cam;
    private Vector3 _direction;
    #endregion

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _cam = Camera.main.transform;
        _direction = new Vector3();
    }

    private void OnEnable() =>
        _input.SetPerformed<InputPerform>(() => Jump(), "Jump");

    private void OnDisable() =>
        _input.UnsetPerformed<InputPerform>(() => Jump(), "Jump");

    private void Update()
    {
        Vector2 vectorDirection = _input.GetVector2Variable("Movement");
        
        //Make a direction math through camera
        _direction = _cam.forward * vectorDirection.y;
        _direction += _cam.right * vectorDirection.x;
        _direction.Normalize();
        _direction.y = 0;
        
        Move();
        Rotate();
    }

    #region Movement Methods
    public void Move()
    {
        Vector3 velocity = -_direction * _data.MoveSpeed;
        _rigidbody.velocity = new Vector3(
            Mathf.Clamp(velocity.x, -1, 1),
            _rigidbody.velocity.y,
            Mathf.Clamp(velocity.z, -1, 1));
    }

    private void Rotate()
    {
        Quaternion targetRotation = transform.rotation;
        
        if (_direction != Vector3.zero)
            targetRotation = Quaternion.LookRotation(_direction);
        
        Quaternion playerRotation = Quaternion.Slerp(transform.rotation, targetRotation, _data.RotationSpeed);
        transform.rotation = playerRotation;
    }
    #endregion

    #region Vertical Methods
    public InputPerform Jump()
    {
        if (IsOnGround())
        {
            Vector3 rbVelocity = _rigidbody.velocity;
            _data.VerticalVelocity = Mathf.Sqrt(_data.JumpForce * -2 * _data.Gravity);
            rbVelocity.y = _data.VerticalVelocity;
            _rigidbody.velocity = rbVelocity;
        }
        
        return new InputPerform();
    }

    private bool IsOnGround()
    {
        return Physics.CheckSphere(_groundCheckPivot.transform.position, 
            _data.GroundCheckRadius, _data.GroundLayers);
    }
    #endregion
}