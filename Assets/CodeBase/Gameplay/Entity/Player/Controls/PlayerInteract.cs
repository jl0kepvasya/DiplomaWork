using System.Linq;
using UnityEngine;
using Zenject;

//Взаимодействие игрока с определенными предметами
public class PlayerInteract : MonoBehaviour
{
    [SerializeField] private EntityInteractConfig _data;
    [SerializeField] private InteractableView _view;
    [Inject] private InputSystem _input;
    
    private Collider[] _interactablesList;
    private bool _interactTrigger;
    
    private void Update()
    {
        _interactablesList = Physics.OverlapSphere(
            transform.position, _data.InteractRadius, _data.InteractablesLayer);

        if (_interactablesList.First(i => i.GetComponent<Interactable>().CanBeInteracted))
        {
            _interactTrigger = _input.GetTriggered("Interact");

            var interactable = _interactablesList.First(i => i.GetComponent<Interactable>()).
                GetComponent<Interactable>();
            
            _view.UpdateMessage(interactable.PromptMessage);
            if (_interactTrigger) 
                interactable.BaseInteract();
        }
        else if (_interactablesList == null || _interactablesList[0] == null || _interactablesList[1] == null)
        {
            _view.UpdateMessage(string.Empty);
        }
    }

    private void OnDrawGizmos() =>
        Gizmos.DrawWireSphere(transform.position, _data.InteractRadius);
}