using TMPro;
using UnityEngine;

public class blyatzaebal : MonoBehaviour
{
    [SerializeField] private InventorySlotView[] _slots;
    [SerializeField] private GameObject[] _skeleton;

    private void Update()
    {
        var slot = _slots[0].GetComponentsInChildren<TMP_Text>();
        foreach (var TMP in slot)
        {
            if (TMP.text == "Bone")
            {
                _skeleton[0].SetActive(true);
            }
            else
            {
                _skeleton[0].SetActive(false);
            }
        }

        var slot1 = _slots[1].GetComponentsInChildren<TMP_Text>();
        foreach (var TMP in slot1)
        {
            if (TMP.text == "Bone")
            {
                _skeleton[1].SetActive(true);
            }
            else
            {
                _skeleton[1].SetActive(false);
            }
        }
        
        var slot2 = _slots[1].GetComponentsInChildren<TMP_Text>();
        foreach (var TMP in slot2)
        {
            if (TMP.text == "Bone")
            {
                _skeleton[2].SetActive(true);
            }
            else
            {
                _skeleton[2].SetActive(false);
            }
        }
        
        var slot3 = _slots[1].GetComponentsInChildren<TMP_Text>();
        foreach (var TMP in slot3)
        {
            if (TMP.text == "Bone")
            {
                _skeleton[3].SetActive(true);
            }
            else
            {
                _skeleton[3].SetActive(false);
            }
        }
        
        var slot4 = _slots[1].GetComponentsInChildren<TMP_Text>();
        foreach (var TMP in slot4)
        {
            if (TMP.text == "Bone")
            {
                _skeleton[4].SetActive(true);
            }
            else
            {
                _skeleton[4].SetActive(false);
            }
        }
    }
}