using System;
using UnityEngine;

//Реализация здоровья
public class Health : IReadOnlyHealth
{
    public event Action<float> HealthChanged;
    public event Action Destroy;
    
    public float StartHealthPoints
    {
        get => _data.StartHealthPoints;
        set
        {
            if (_data.StartHealthPoints != value)
            {
                _data.StartHealthPoints = value;
            }
        }
    }
    
    public float CurrentHealthPoints
    {
        get => _data.CurrentHealthPoints;
        set
        {
            if (_data.CurrentHealthPoints != value)
            {
                _data.CurrentHealthPoints = value;
                HealthChanged?.Invoke(value);

                if (_data.CurrentHealthPoints == 0)
                    Destroy?.Invoke();
            }
        }
    }
    
    private readonly HealthData _data;

    public Health(HealthData data) => _data = data;

    public void ApplyDamage(float damage) =>
        CurrentHealthPoints = Mathf.Clamp(CurrentHealthPoints - damage, 0, _data.StartHealthPoints);
    
    public void ApplyHeal(float heal) =>
        CurrentHealthPoints = Mathf.Clamp(CurrentHealthPoints + heal, 0, _data.StartHealthPoints);
}