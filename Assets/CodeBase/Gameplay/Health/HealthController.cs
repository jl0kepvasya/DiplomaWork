//Контроллер здоровья, объединяющий View составляющую и Model составляющую

using UnityEngine.SceneManagement;

public class HealthController
{
    private readonly HealthView _view;

    public HealthController(IReadOnlyHealth health, HealthView view)
    {
        _view = view;

        //Subscribing to event actions
        health.HealthChanged += OnHealthChanged;
        health.Destroy += OnDestroy;
        
        //Setting the current hp
        _view.CurrentHealthPoints = health.CurrentHealthPoints;
    }

    private void OnHealthChanged(float currentHealth)
    {
        _view.CurrentHealthPoints = currentHealth;
    }

    private void OnDestroy()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}