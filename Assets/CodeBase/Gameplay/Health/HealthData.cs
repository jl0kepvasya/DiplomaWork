using System;

//Данные здоровья
[Serializable]
public class HealthData
{
    public float StartHealthPoints;
    public float CurrentHealthPoints;
}