using UnityEngine;

//Точка входа для здоровья на игроке
public class HealthEntryPoint : MonoBehaviour
{
    [SerializeField] private float _startHealth;
    [SerializeField] private HealthView _healthView;

    private Health _health;
    
    private void Start()
    {
        HealthData healthData = new HealthData()
        {
            StartHealthPoints = _startHealth,
            CurrentHealthPoints = _startHealth
        };
        
        _health = new Health(healthData);

        HealthController controller = new HealthController(_health, _healthView);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DamageDealer"))
        {
            _health.ApplyDamage(20);
        }

        if (other.CompareTag("Healer"))
        {
            _health.ApplyHeal(10);
            other.gameObject.SetActive(false);
        }
    }
}