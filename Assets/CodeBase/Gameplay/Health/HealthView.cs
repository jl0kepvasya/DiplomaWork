using System;
using TMPro;
using UnityEngine;

//Отображение здоровья
public class HealthView : MonoBehaviour
{
    [SerializeField] private TMP_Text _currentHealthPoints;
    
    public float CurrentHealthPoints
    {
        get => Convert.ToInt32(_currentHealthPoints.text);
        set => _currentHealthPoints.text = value.ToString();
    }
}