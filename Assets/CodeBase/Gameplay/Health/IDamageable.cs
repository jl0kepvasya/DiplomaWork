//Интерфейс для получения урона
public interface IDamageable
{
    void ApplyDamage(float damage);
}