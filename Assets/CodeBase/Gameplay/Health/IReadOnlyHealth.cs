using System;

//Интерфейс для последующей реализации здоровья
public interface IReadOnlyHealth : IDamageable
{
    event Action<float> HealthChanged;
    event Action Destroy;
    
    float StartHealthPoints { get; }
    float CurrentHealthPoints { get; }
}