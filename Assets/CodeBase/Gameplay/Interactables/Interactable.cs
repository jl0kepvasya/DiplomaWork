using UnityEngine;

//Абстрактный класс для реализации предметов, с которыми можно взаимодействовать
public abstract class Interactable : MonoBehaviour
{
    [SerializeField] public bool CanBeInteracted;
    [SerializeField] public string PromptMessage;
    public void BaseInteract() => Interact();
    protected abstract void Interact();
}