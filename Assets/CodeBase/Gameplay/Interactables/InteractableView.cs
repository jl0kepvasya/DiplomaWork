using TMPro;
using UnityEngine;

//Отображение текста и "индикатор" возможности взаимодействия с объектом
public class InteractableView : MonoBehaviour
{
    [SerializeField] private TMP_Text _promptMessageText;
    
    public void UpdateMessage(string promptMessage) => _promptMessageText.text = promptMessage;
}