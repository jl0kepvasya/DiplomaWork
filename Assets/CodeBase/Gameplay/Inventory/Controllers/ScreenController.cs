//Контроллер окна инвентарей
public class ScreenController
{
    private readonly ScreenView _view;
    private readonly InventoriesService _service;

    private InventoryGridController _currentInventoryController;
    
    public ScreenController(
        ScreenView view, InventoriesService service)
    {
        _view = view;
        _service = service;
    }

    public InputPerform InventoryControl(string ownerId)
    {
        if (!_view.isActiveAndEnabled)
            OpenInventory(ownerId);
        else
            CloseInventory();
        
        return new InputPerform();
    }
    
    public void OpenInventory(string ownerId)
    {
        //Optimize? Not generate each time but call it
        var inventory = _service.GetInventory(ownerId);
        var inventoryView = _view.InventoryView;

        _currentInventoryController = new InventoryGridController(inventory, inventoryView);
        
        _view.SetActive(true);
    }

    public void CloseInventory() =>
        _view.SetActive(false);
}