//Структура добавления вещей в инвентарь
public readonly struct AddItemsToInventoryResult
{
    public readonly string InventoryOwnerId;
    public readonly int ItemsToAddAmount;
    public readonly int ItemsAddedAmount;

    public int ItemsNotAdded => ItemsToAddAmount - ItemsAddedAmount;

    public AddItemsToInventoryResult(
        string inventoryOwnerId,
        int itemsToAddAmount,
        int itemsAddedAmount)
    {
        InventoryOwnerId = inventoryOwnerId;
        ItemsToAddAmount = itemsToAddAmount;
        ItemsAddedAmount = itemsAddedAmount;
    }
}