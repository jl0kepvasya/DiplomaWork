using System;
using System.Collections.Generic;
using UnityEngine;

//Данные об инвентаре
[Serializable]
public class InventoryGridData
{
    public string OwnerId;
    public Vector2Int Size;
    public List<InventorySlotData> Slots;
}