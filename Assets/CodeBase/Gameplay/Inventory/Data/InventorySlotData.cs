using System;

//Данные о ячейке инвентаря
[Serializable]
public class InventorySlotData
{
    public string ItemId;
    public int Amount;
}