//Структура удаления вещей из инвентаря
public readonly struct RemoveItemsFromInventoryResult
{
    public readonly string InventoryOwnerId;
    public readonly int ItemsToRemove;
    public readonly bool Success;

    public RemoveItemsFromInventoryResult(
        string inventoryOwnerId,
        int itemsToRemove,
        bool success)
    {
        InventoryOwnerId = inventoryOwnerId;
        ItemsToRemove = itemsToRemove;
        Success = success;
    }
}