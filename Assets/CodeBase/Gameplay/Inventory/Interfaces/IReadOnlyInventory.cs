using System;
using UnityEngine;

//Интерфейс инвентаря для основы и последующей реализации
public interface IReadOnlyInventory
{
    event Action<string, int> ItemsAdded;
    event Action<string, int> ItemsRemoved;
    event Action<Vector2Int> SizeChanged; 
        
    string OwnerId { get; }
    Vector2Int Size { get; }

    int GetAmount(string itemId);
    bool Has(string itemId, int amount);
    IReadOnlyInventorySlot[,] GetSlots();
}