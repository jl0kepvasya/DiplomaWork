using System.Collections.Generic;
using UnityEngine;
using Zenject;

//Точка входа в инвентарь для игрока
public class InventoryEntryPoint : MonoBehaviour
{
    [SerializeField] private string _ownerId;
    [SerializeField] private ScreenView _screenView;
    
    [Inject] private InventoriesService _inventoriesService;
    [Inject] private InputSystem _input;
    [Inject] private Dictionary<string, ItemSO> _items;
    
    private InventorySlots _inventorySlots;
    private ScreenController _screenController;
    
    private void Awake()
    {
        _inventorySlots = new InventorySlots();
        
        var invData = _inventorySlots.CreateInventory(_ownerId, new Vector2Int(3,4));
        _inventoriesService.RegisterInventory(invData, _items);

        _screenController = new ScreenController(_screenView, _inventoriesService);
    }

    private void OnEnable() =>
        _input.SetPerformed<InputPerform>(() => _screenController.InventoryControl(_ownerId),
            "InventoryActions");

    private void OnDisable() =>
        _input.UnsetPerformed<InputPerform>(() => _screenController.InventoryControl(_ownerId),
            "InventoryActions");
}