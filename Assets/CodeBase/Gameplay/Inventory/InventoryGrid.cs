using System;
using System.Collections.Generic;
using UnityEngine;

//Реализация инвентаря и всех его методов, алгоритмов
public class InventoryGrid : IReadOnlyInventory
{ 
    public event Action<string, int> ItemsAdded;
    public event Action<string, int> ItemsRemoved;
    public event Action<Vector2Int> SizeChanged;

    public string OwnerId => _data.OwnerId;
    public Vector2Int Size
    {
        get => _data.Size;
        set
        {
            if (_data.Size != value)
            {
                _data.Size = value;
                SizeChanged?.Invoke(value);
            }
        }
    }
    
    private readonly InventorySlot[,] _slots; 
    protected readonly InventoryGridData _data; 
    protected readonly Dictionary<string, ItemSO> _items;
    
    //Initialize inventory
    public InventoryGrid(InventoryGridData data, Dictionary<string, ItemSO> items)
    {
        _data = data;
        _items = items;

        Vector2Int size = data.Size;
        _slots = new InventorySlot[size.x, size.y];
            
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                int index = i * size.y + j;
                var slotData = data.Slots[index];
                var slot = new InventorySlot(slotData);
                _slots[i, j] = slot;
            }
        }
    }
    
    public int GetAmount(string itemId)
    {
        var amount = 0;
        var slots = _data.Slots;

        foreach (var slot in slots)
        {
            if (slot.ItemId == itemId)
                amount += slot.Amount;
        }

        return amount;
    }
    
    //Check inventory has slots with the amount of items
    public bool Has(string itemId, int amount)
    {
        var amountExist = GetAmount(itemId);
        return amountExist >= amount;
    }
    
    public void SwitchSlots(Vector2Int slotCoordsA, Vector2Int slotCoordsB)
    {
        var slotA = _slots[slotCoordsA.x, slotCoordsA.y];
        var slotB = _slots[slotCoordsB.x, slotCoordsB.y];

        var tempSlotItemId = slotA.ItemId;
        var tempSlotAmount = slotA.Amount;

        slotA.ItemId = slotB.ItemId;
        slotA.Amount = slotB.Amount;
        slotB.ItemId = tempSlotItemId;
        slotB.Amount = tempSlotAmount;
    }
    
    //Creating cache(copy) to not give redact the original inventory
    public IReadOnlyInventorySlot[,] GetSlots()
    {
        IReadOnlyInventorySlot[,] cachedSlots = _slots;
        return cachedSlots;
    }
    
    //Filling items to free slots
    public AddItemsToInventoryResult AddItems(string itemId, int amount = 1)
    {
        var remainingAmount = amount;
        var sameItemsAddedAmount = AddToSameSlots(itemId, remainingAmount, out remainingAmount);

        if (remainingAmount <= 0)
            return new AddItemsToInventoryResult(OwnerId, amount, sameItemsAddedAmount);

        var addedToAvailableSlots = AddToFirst(itemId, remainingAmount, out remainingAmount);
        var totalAddedItemsAmount = sameItemsAddedAmount + addedToAvailableSlots;
            
        return new AddItemsToInventoryResult(OwnerId, amount, totalAddedItemsAmount);
    }
    
    //Filling items to the slot (slotCoordinates)
    public AddItemsToInventoryResult AddItems(Vector2Int slotCoords, string itemId, int amount = 1)
    {
        var slot = _slots[slotCoords.x, slotCoords.y];
        int newValue = slot.Amount + amount;
        int itemsAddedAmount = 0;

        if (slot.IsEmpty)
            slot.ItemId = itemId;

        int itemSlotCapacity = GetItemSlotCapacity(itemId);

        if (newValue > itemSlotCapacity)
        {
            int remainingItem = newValue - itemSlotCapacity;
            int itemsToAddAmount = itemSlotCapacity - slot.Amount;
            itemsAddedAmount += itemsToAddAmount;
            slot.Amount = itemSlotCapacity;

            var result = AddItems(itemId, remainingItem);
            itemsAddedAmount += result.ItemsAddedAmount;
        }
        else
        {
            itemsAddedAmount = amount;
            slot.Amount = newValue;
        }

        return new AddItemsToInventoryResult(OwnerId, amount, itemsAddedAmount);
    }
    
    //Remove items if all slots filled
    public RemoveItemsFromInventoryResult RemoveItems(string itemId, int amount = 1)
    {
        if (!Has(itemId, amount))
            return new RemoveItemsFromInventoryResult(OwnerId, amount, false);

        var amountToRemove = amount;

        for (int i = 0; i < Size.x; i++)
        {
            for (int j = 0; j < Size.y; j++)
            {
                var slotCoords = new Vector2Int(i, j);
                var slot = _slots[i, j];
                    
                if (slot.ItemId != itemId)
                    continue;

                if (amountToRemove > slot.Amount)
                {
                    amountToRemove -= slot.Amount;

                    RemoveItems(slotCoords, itemId, slot.Amount);
                }
                else
                {
                    RemoveItems(slotCoords, itemId, amountToRemove);

                    return new RemoveItemsFromInventoryResult(OwnerId, amount, true);
                }
            }
        }

        throw new Exception("Something went wrong. Couldn't remove items");
    }
    
    //Remove items from the slot (slotCoordinates)
    public RemoveItemsFromInventoryResult RemoveItems(Vector2Int slotCoords, string itemId, int amount = 1)
    {
        var slot = _slots[slotCoords.x, slotCoords.y];

        if (slot.IsEmpty || slot.ItemId != itemId || slot.Amount < amount)
            return new RemoveItemsFromInventoryResult(OwnerId, amount, false);

        slot.Amount -= amount;

        if (slot.Amount == 0)
            slot.ItemId = null;

        return new RemoveItemsFromInventoryResult(OwnerId, amount, true);
    }
    
    //Add items to the same found item slot 
    public int AddToSameSlots(string itemId, int amount, out int remainingAmount)
    {
        var itemsAddedAmount = 0;
        remainingAmount = amount;

        for (int i = 0; i < Size.x; i++)
        {
            for (int j = 0; j < Size.y; j++)
            {
                var slot = _slots[i, j];
                    
                if (slot.IsEmpty)
                    continue;

                int slotItemCapacity = GetItemSlotCapacity(slot.ItemId);
                    
                if (slot.Amount >= slotItemCapacity)
                    continue;
                if (slot.ItemId != itemId)
                    continue;
                    
                int newValue = slot.Amount + remainingAmount;

                if (newValue > slotItemCapacity)
                {
                    remainingAmount = newValue - slotItemCapacity;
                    int itemsToAddAmount = slotItemCapacity - slot.Amount;
                    itemsAddedAmount += itemsToAddAmount;
                    slot.Amount = slotItemCapacity;

                    if (remainingAmount == 0)
                        return itemsAddedAmount;
                }
                else
                {
                    itemsAddedAmount += remainingAmount;
                    slot.Amount = newValue;
                    remainingAmount = 0;

                    return itemsAddedAmount;
                }
            }
        }

        return itemsAddedAmount;
    }
    
    //Add items to first founded slot
    public int AddToFirst(string itemId, int amount, out int remainingAmount)
    {
        var itemsAddedAmount = 0;
        remainingAmount = amount;

        for (int i = 0; i < Size.x; i++)
        {
            for (int j = 0; j < Size.y; j++)
            {
                var slot = _slots[i, j];
                    
                if (!slot.IsEmpty)
                    continue;

                slot.ItemId = itemId;
                int newValue = remainingAmount;
                int slotItemCapacity = GetItemSlotCapacity(slot.ItemId);

                if (newValue > slotItemCapacity)
                {
                    remainingAmount = newValue - slotItemCapacity;
                    int itemsToAddAmount = slotItemCapacity;
                    itemsAddedAmount += itemsToAddAmount;
                    slot.Amount = slotItemCapacity;
                }
                else
                {
                    itemsAddedAmount += remainingAmount;
                    slot.Amount = newValue;
                    remainingAmount = 0;

                    return itemsAddedAmount;
                }
            }
        }

        return itemsAddedAmount;
    }
    
    //Maximum amount in slot to item
    public virtual int GetItemSlotCapacity(string itemId)
    {
        return _items[itemId].Capacity;
    }
}