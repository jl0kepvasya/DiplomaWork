using System.Collections.Generic;
using UnityEngine;

//Создание/инициализация инвентаря
public class InventorySlots
{
    //Create Inventory
    public InventoryGridData CreateInventory(string ownerId, Vector2Int size)
    {
        var createdInventorySlots = new List<InventorySlotData>();
        var length = size.x * size.y;

        for (int i = 0; i < length; i++)
            createdInventorySlots.Add(new InventorySlotData());

        var createdInventoryData = new InventoryGridData
        {
            OwnerId = ownerId,
            Size = size,
            Slots = createdInventorySlots
        };

        return createdInventoryData;
    }
}