using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

//Отображение ячейки инвентаря и взаимодействие с ней
public class InventorySlotView : MonoBehaviour, IDropHandler
{
    [SerializeField] private TMP_Text _itemTextTitle;
    [SerializeField] private TMP_Text _itemTextAmount;
 
    public event Action<InventorySlotView> ItemTookFromSlot;
    public event Action<InventorySlotView> ItemDroppedInSlot;
    
    public string Title
    {
        get => _itemTextTitle.text;
        set => _itemTextTitle.text = value;
    }

    public int Amount
    {
        get => Convert.ToInt32(_itemTextAmount.text);
        set => _itemTextAmount.text = value == 0 ? string.Empty : value.ToString();
    }

    private void Update()
    {
        if (gameObject.GetComponentInChildren<InventoryItemView>() == null)
            ItemTookFromSlot?.Invoke(this);
    }

    public void OnDrop(PointerEventData eventData)
    {
        InventoryItemView item = eventData.pointerDrag.gameObject.GetComponent<InventoryItemView>();
        item._parentAfterDrag = transform;
        ItemDroppedInSlot?.Invoke(this);
    }
}