using System;
using System.Linq;
using TMPro;
using UnityEngine;
using Zenject;

//Отображение инвентаря и его ячеек
public class InventoryView : MonoBehaviour
{
    [SerializeField] private InventorySlotView[] _inventorySlots;
    [SerializeField] public InventorySlotView[] _additionalSlots;
    [SerializeField] private TMP_Text _invTextOwner;

    [Inject] private InventoriesService _inventoriesService;
    
    private InventorySlotView _inventorySlot;
    private InventorySlotView[] _slots;
    
    public string OwnerId
    {
        get => _invTextOwner.text;
        set => _invTextOwner.text = value;
    }

    public void Setup()
    {
        int length = _inventorySlots.Length + _additionalSlots.Length;
        _slots = new InventorySlotView[length];
        
        length = _inventorySlots.Length;
        for (int i = 0; i < length; i++)
            _slots[i] = _inventorySlots[i];

        length = _slots.Length;
        for (int i = _inventorySlots.Length; i < length; i++)
            _slots[i] = _additionalSlots[i - _inventorySlots.Length];
    }

    protected void OnEnable()
    {
        foreach (var slot in _slots)
        {
            slot.ItemTookFromSlot += OnItemTookFromSlot;
            slot.ItemDroppedInSlot += OnItemDroppedInSlot;
        }
    }

    protected void OnDisable()
    {
        foreach (var slot in _slots)
        {
            slot.ItemTookFromSlot -= OnItemTookFromSlot;
            slot.ItemDroppedInSlot -= OnItemDroppedInSlot;
        }
    }

    protected void OnItemTookFromSlot(InventorySlotView slot)
    {
        _inventorySlot = _slots.First(s => s == slot);
    }
    
    protected void OnItemDroppedInSlot(InventorySlotView slot)
    {
        var droppedInInventorySlot = _slots.First(s => s == slot);
        
        var inventory = _inventoriesService.GetInventory("Player");
        var slots = inventory.GetSlots();
        Vector2Int slotCoordsA = new Vector2Int();
        Vector2Int slotCoordsB = new Vector2Int();
        
        for (int i = 0; i < inventory.Size.x; i++)
        {
            for (int j = 0; j < inventory.Size.y; j++)
            {
                if (slots[i, j].ItemId == _inventorySlot.Title)
                {
                    slotCoordsA = new Vector2Int(i, j);
                }

                if (slots[i, j].ItemId == droppedInInventorySlot.Title)
                {
                    slotCoordsB = new Vector2Int(i, j);
                }
            }
        }
        
        _inventoriesService.SwitchSlots("Player", slotCoordsA, slotCoordsB);
    }

    public InventorySlotView GetInventorySlotView(int index)
    {
        return _slots[index];
    }
}