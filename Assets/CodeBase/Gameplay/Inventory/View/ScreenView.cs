using UnityEngine;
using Zenject;

//Отображение окна инвентаря
public class ScreenView : MonoBehaviour
{
    [SerializeField] private InventoryView _inventoryView;
    [Inject] private CameraRotation _camRotation;
    
    public InventoryView InventoryView => _inventoryView;

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
        _camRotation.enabled = !active;
    }
}