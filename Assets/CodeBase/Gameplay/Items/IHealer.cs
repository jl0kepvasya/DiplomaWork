public interface IHealer
{
    public float Heal { get; }

    public void SetHeal(float heal);
}