using UnityEngine;

public class Item_Healer : Interactable, IHealer
{
    [SerializeField] private float _heal;

    public float Heal
    {
        get => Heal;
        set
        {
            if (Heal != value)
                Heal = value;
        }
    }

    protected override void Interact()
    {
        SphereCollider collider = new SphereCollider();
        var transformCollider = collider.transform;
        var gameObjCollider = collider.gameObject;
        
        gameObjCollider.SetActive(true);
        gameObjCollider.tag = "Healer";
        transformCollider.position = FindObjectOfType<PlayerInteract>().transform.position;
        
        gameObject.SetActive(false);
    }

    public void SetHeal(float heal) => Heal = _heal;
}