using UnityEngine;
using Zenject;

//Предмет взаимодействия, который кладется в инвентарь
public class Item : Interactable
{
    [SerializeField] public ItemSO _item;
    [Inject] private InventoriesService _service;

    protected override void Interact()
    {
        gameObject.SetActive(false);
        _service.AddItemsToInventory("Player", _item.Id);
    }
}
