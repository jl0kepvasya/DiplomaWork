using UnityEngine;

//Предмет блин
[CreateAssetMenu(fileName = "ItemData", menuName = "Interactables/Item")]
public class ItemSO : ScriptableObject
{
    [SerializeField] public string Id;
    [SerializeField] public int Capacity;
}