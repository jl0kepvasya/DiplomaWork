using System;

public interface IReadOnlyLevel
{
    public event Action<int> LevelChanged;
    public event Action<float> ProgressChanged;
    public event Action<float> XPToLevelUpChanged;

    public float LevelProgress { get; }
    public float XPToLevelUp { get; }
    public int CurrentLevel { get; }
}