using System;
using UnityEngine;

public class Level : IReadOnlyLevel
{
    public event Action<int> LevelChanged;
    public event Action<float> ProgressChanged;
    public event Action<float> XPToLevelUpChanged;
    
    public float LevelProgress
    {
        get => _data.LevelProgress;
        set
        {
            if (_data.LevelProgress != value)
            {
                _data.LevelProgress = value;
                ProgressChanged?.Invoke(value);
            }
        }
    }
    
    public float XPToLevelUp
    {
        get => _data.XPToLevelUp;
        set
        {
            if (_data.XPToLevelUp != value)
            {
                _data.XPToLevelUp = value;
                XPToLevelUpChanged?.Invoke(value);
            }
        }
    }

    public int CurrentLevel
    {
        get => _data.CurrentLevel;
        set
        {
            if (Mathf.Abs(_data.LevelProgress) == Mathf.Abs(_data.XPToLevelUp))
            {
                ++CurrentLevel;
                LevelChanged?.Invoke(CurrentLevel);
            }
        }
    }

    public LevelData _data;

    public Level(LevelData data)
    {
        _data = data;
    }

    public void NewXPLimit()
    {
        XPToLevelUp *= 2;
    }

    public void ProgressAddXP(float xp)
    {
        LevelProgress += xp;

        if (LevelProgress >= XPToLevelUp)
        {
            float remainXP = LevelProgress - XPToLevelUp;
            NewXPLimit();
            ProgressAddXP(remainXP);
        }
    }
}