public class LevelController
{
    private LevelView _view;
    
    public LevelController(IReadOnlyLevel level, LevelView view)
    {
        _view = view;

        level.LevelChanged += OnLevelChanged;
        level.ProgressChanged += OnProgressLevelChanged;
        level.XPToLevelUpChanged += OnXPToLevelUpChanged;

        _view.CurrentLevel = level.CurrentLevel;
        _view.LevelProgress = level.LevelProgress;
        _view.XPToLevelUp = level.XPToLevelUp;
    }

    private void OnLevelChanged(int newLevel)
    {
        _view.CurrentLevel = newLevel;
    }

    private void OnProgressLevelChanged(float progress)
    {
        _view.LevelProgress = progress;
    }

    private void OnXPToLevelUpChanged(float newLimit)
    {
        _view.XPToLevelUp = newLimit;
    }
}