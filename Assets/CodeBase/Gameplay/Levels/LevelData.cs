using System;

//Данные о уровне
[Serializable]
public class LevelData
{
    public float LevelProgress;
    public float XPToLevelUp;
    public int CurrentLevel;
}