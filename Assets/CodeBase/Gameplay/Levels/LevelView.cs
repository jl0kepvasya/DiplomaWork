using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelView : MonoBehaviour
{
    [SerializeField] private TMP_Text _levelText;
    [SerializeField] private Image _levelProgress;
    [SerializeField] private TMP_Text _xpToLevelUpText;

    public int CurrentLevel
    {
        get => Convert.ToInt32(_levelText.text);
        set => _levelText.text = value.ToString();
    }
    
    public float LevelProgress
    {
        get => _levelProgress.fillAmount;
        set => _levelProgress.fillAmount = value;
    }

    public float XPToLevelUp
    {
        get => Convert.ToInt32(_xpToLevelUpText.text);
        set => _xpToLevelUpText.text = value.ToString();
    }
}