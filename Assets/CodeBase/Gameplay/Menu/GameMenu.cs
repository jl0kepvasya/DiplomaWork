using UnityEngine;
using Zenject;

public class GameMenu : MonoBehaviour
{
    [Inject] private InputSystem _input;

    private void Update()
    {
        bool escTriggered = _input.GetTriggered("Exit");
        
        if (escTriggered)
            Application.Quit();
    }
}