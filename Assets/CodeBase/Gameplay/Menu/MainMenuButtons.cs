using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

//Скрипт главного меню игры
public class MainMenuButtons : MonoBehaviour
{
    private ISaveLoadService _saveLoadService;
    [Inject] private GameObject _settingsMenu;
    
    public void StartButton() =>
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    public void Continue()
    {
        
    }
    
    public void SettingsButton()
    {
        _settingsMenu.SetActive(!_settingsMenu.activeInHierarchy);
    }
    
    public void QuitButton() =>
        Application.Quit();
}
