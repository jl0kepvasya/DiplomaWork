public interface IWeapon
{
    float Damage { get; }
}