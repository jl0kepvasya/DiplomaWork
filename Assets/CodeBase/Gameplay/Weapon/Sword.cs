using UnityEngine;

public class Sword : MonoBehaviour, IWeapon
{
    [SerializeField] private float damage;
    
    public float Damage
    {
        get => Damage;
        set
        {
            if (Damage != value)
                Damage = value;
        }
    }
    
    private void OnEnable()
    {
        Damage = damage;
    }
}