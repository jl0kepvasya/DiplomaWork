using Zenject;

//Инъектор для сервиса инвентарей
public class InventoriesServiceInstaller : Installer<InventoriesServiceInstaller>
{
    private InventoriesService _inventoriesService = new InventoriesService();
        
    public override void InstallBindings()
    {
        Container.Bind<InventoriesService>().FromInstance(_inventoriesService);
    }
}