using System.Collections.Generic;
using UnityEngine;
using Zenject;

//Инъектор для списка предметов
public class ItemInstaller : MonoInstaller
{
    [SerializeField] private List<ItemSO> itemsList;
    private Dictionary<string, ItemSO> itemsDic = new Dictionary<string, ItemSO>();

    private void Awake()
    {
        foreach (var item in itemsList)
            itemsDic.Add(item.Id, item);
    }

    public override void InstallBindings()
    {
        Container.Bind<Dictionary<string, ItemSO>>().FromInstance(itemsDic).AsSingle();
    }
}