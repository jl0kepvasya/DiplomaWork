using UnityEngine;
using Zenject;

public class MainMenuInstaller : MonoInstaller
{
    [SerializeField] private GameObject _settingsContainer;
    
    public override void InstallBindings()
    {
        Container.Bind<GameObject>().FromInstance(_settingsContainer);
    }
}