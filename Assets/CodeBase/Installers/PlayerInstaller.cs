using UnityEngine;
using Zenject;

//Загрузчик, инъектор зависимостей от фреймворка Zenject и для взаимодействия с игроком
public class PlayerInstaller : MonoInstaller
{
    [SerializeField] private InputSystem Input;
    [SerializeField] private PlayerInteract Interact;
    [SerializeField] private Transform PlayerGameObject;
    [SerializeField] private CameraRotation CameraRotation;
    
    public override void InstallBindings()
    {
        Container.Bind<InputSystem>().FromInstance(Input);
        Container.Bind<PlayerInteract>().FromInstance(Interact);
        Container.Bind<Transform>().FromInstance(PlayerGameObject).AsSingle();
        Container.Bind<CameraRotation>().FromInstance(CameraRotation).AsSingle();
    }
}