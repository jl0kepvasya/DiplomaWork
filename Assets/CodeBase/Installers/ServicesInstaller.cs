using Zenject;

//Инъектор сервисов
public class ServicesInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        InventoriesServiceInstaller.Install(Container);
    }
}