using System;
using UnityEngine;

//Система ввода
public class InputSystem : MonoBehaviour
{
    private PlayerControls _playerControls;
    private PlayerControls.GameplayActions _gameplayActions;
    private void OnEnable()
    {
        _playerControls ??= new PlayerControls();
        _gameplayActions = _playerControls.Gameplay;
        _playerControls.Enable();
    }

    public Vector2 GetVector2Variable(string actionName)
    {
        return _gameplayActions.Get()[actionName].ReadValue<Vector2>();
    }

    public bool GetTriggered(string actionName)
    {
        return _gameplayActions.Get()[actionName].triggered;
    }
    
    /// <summary>
    /// Setting any method performed to new input system 
    /// </summary>
    /// <param name="func">Any method/function. Needs to be "() => Method(params)"</param>
    /// <param name="actionName"></param>
    /// <typeparam name="T"></typeparam>
    public void SetPerformed<T>(Func<T> func, string actionName) where T : struct
    {
        _gameplayActions.Get()[actionName].performed += obj => func?.Invoke();
    }
    
    public void UnsetPerformed<T>(Func<T> func, string actionName) where T : struct
    {
        _gameplayActions.Get()[actionName].performed -= obj => func?.Invoke();
    }
}
