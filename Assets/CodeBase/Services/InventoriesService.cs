using System.Collections.Generic;
using UnityEngine;

//Сервис инвентарей, хранящий в себе все имеющиеся инвентари
public class InventoriesService
{
    private readonly Dictionary<string, InventoryGrid> _inventoriesMap = new();
    
    public InventoryGrid RegisterInventory(InventoryGridData inventoryGridData, Dictionary<string, ItemSO> items)
    {
        var inventory = new InventoryGrid(inventoryGridData, items);
        _inventoriesMap[inventory.OwnerId] = inventory;

        return inventory;
    }

    public IReadOnlyInventory GetInventory(string ownerId)
    {
        return _inventoriesMap[ownerId];
    }

    public void SwitchSlots(
        string ownerId, Vector2Int slotCoordsA, Vector2Int slotCoordsB)
    {
        var inventory = _inventoriesMap[ownerId];
        inventory.SwitchSlots(slotCoordsA, slotCoordsB);
    }

    public AddItemsToInventoryResult AddItemsToInventory(
        string ownerId, string itemId, int amount = 1)
    {
        var inventory = _inventoriesMap[ownerId];
        return inventory.AddItems(itemId, amount);
    }
    
    public AddItemsToInventoryResult AddItemsToInventorySlot(
        string ownerId, Vector2Int slotCoords, string itemId, int amount = 1)
    {
        var inventory = _inventoriesMap[ownerId];
        return inventory.AddItems(slotCoords, itemId, amount);
    }
    
    public RemoveItemsFromInventoryResult RemoveItemsFromInventory(
        string ownerId, string itemId, int amount = 1)
    {
        var inventory = _inventoriesMap[ownerId];
        return inventory.RemoveItems(itemId, amount);
    }

    public RemoveItemsFromInventoryResult RemoveItemsFromInventorySlot(
        string ownerId, Vector2Int slotCoords, string itemId, int amount = 1)
    {
        var inventory = _inventoriesMap[ownerId];
        return inventory.RemoveItems(slotCoords, itemId, amount);
    }

    public bool Has(string ownerId, string itemId, int amount)
    {
        var inventory = _inventoriesMap[ownerId];
        return inventory.Has(itemId, amount);
    }
}