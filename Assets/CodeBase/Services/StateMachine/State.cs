public abstract class State
{
    protected readonly StateMachine FSM;

    public State(StateMachine fsm) => FSM = fsm;
    public abstract void Enter();
    public virtual void Update() {}
    public virtual void Exit() {}
}