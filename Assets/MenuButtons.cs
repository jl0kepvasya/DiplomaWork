using UnityEngine;
using Zenject;

public class MenuButtons : MonoBehaviour
{
    private ISaveLoadService _saveLoadService;
    [SerializeField] private GameObject _menu;
    [SerializeField] private InventoryEntryPoint _inventoryEntryPoint;
    [SerializeField] private HealthEntryPoint _healthEntryPoint;
    
    [Inject] private InputSystem _input;

    private void Awake()
    {
        _saveLoadService = new JsonToFileSaveLoadService();
    }

    private void Update()
    {
        var esc = _input.GetTriggered("Menu");
        
        if (esc == true)
            _menu.SetActive(true);
        else
            _menu.SetActive(false);
    }

    public void ContinueButton()
    {
        _menu.SetActive(false);
    }

    public void SaveButton()
    {
        _saveLoadService.Save("somekey", _healthEntryPoint);
        _saveLoadService.Save("somekey", _inventoryEntryPoint);
    }
    
    public void QuitButton() =>
        Application.Quit();
}
